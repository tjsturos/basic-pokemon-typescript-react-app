"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Sidebar = /** @class */ (function (_super) {
    __extends(Sidebar, _super);
    function Sidebar() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Sidebar.prototype.buttonClick = function (id) {
        console.log("Lookup pokemon number ", id);
    };
    Sidebar.prototype.displayList = function (list) {
        var _this = this;
        return list.map(function (pokemon, index) {
            return (React.createElement("button", { key: index, className: "sidebar__list-item", onClick: function () { return _this.buttonClick(pokemon.id); } }, pokemon.name));
        });
    };
    Sidebar.prototype.render = function () {
        return (React.createElement("div", { className: "sidebar" },
            React.createElement("h1", null, "Sidebar"),
            React.createElement("div", { className: "list" }, this.displayList(this.props.pokemonList))));
    };
    return Sidebar;
}(React.Component));
exports.default = Sidebar;
//# sourceMappingURL=Sidebar.js.map