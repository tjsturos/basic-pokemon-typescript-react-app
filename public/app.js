"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
require("normalize.css/normalize.css");
require("./styles/styles.scss");
var Layout_1 = require("./components/Layout");
ReactDOM.render(React.createElement(Layout_1.default, null), document.getElementById('app'));
//# sourceMappingURL=app.js.map