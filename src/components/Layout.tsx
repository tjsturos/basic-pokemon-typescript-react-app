'use strict'
import * as React from "react";
import Sidebar from './Sidebar';
import Feature from './Feature';
import { PokemonListItem } from './Definitions';
import request = require('request');

import {Request, CoreOptions, ResponseAsJSON} from 'request';


const BASE_URL: string = "https://sheltered-sea-83101.herokuapp.com/";
export default class Layout extends React.Component {
    state = {
        count: 30,
        list: [{id: 1, name: "pokemon 1", url: "abc.123"}, {id: 2, name: "pokemon 2", url: "890.xyz"}],
        loading: true,
        pokemon: {name: "test pokemon", habitat: "the computer", color: "varies on time of day", picture: ""}
    }

    async componentWillMount() {

        let body: string = "{ listPokemon(page: 0) { count, pokemon { id, name }}}";
        
        let options: CoreOptions = {
            body,
            headers: { 'Content-Type': 'application/graphql' },
        }

        let res: Request = request.post(BASE_URL, options, (res) => {
            let json: ResponseAsJSON = res.toJSON();
            this.setState((prevState) => ({
                count: json.body.data.count,
                pokemon: json.body.data.results
            }), () => {
                console.log("Loading is done.")
                this.setState(() => ({ loading: false }))
            })
        });
    
        

        
        
    }

    changePokemon(id: number): number  {
        console.log("Change to Pokemon number ", id);
        return id;
    }
    render() {
        return (
            <div className="grid-container">
                <Sidebar count={this.state.count} pokemonList={this.state.list} changePokemon={this.changePokemon} />
                <div className="divider"> </div>
                <Feature pokemon={this.state.pokemon} />
            </div>
        );
    }
}