const path = require('path');

module.exports = {
    entry: './src/app.tsx',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        
        rules: [
        {
            test: /\.(jpg|png|svg)$/,
            use: {
                loader: "url-loader",
            },
            include: path.join(__dirname, 'public', 'images')
        },
        {
            loader: 'awesome-typescript-loader',
            test: /\.tsx?$/,
            exclude: /node_modules/
        }, 
        { 
            loader: 'source-map-loader',
            enforce: 'pre',
            test: /\.jsx?$/
        },
        {
            test: /\.s?css$/,
            use: [
                'style-loader',
                'css-loader',
                'sass-loader'
            ] 
        }, {

        }]
    }, 
    resolve: {
        // needed to resolve ("find") files that end with these extensions
        extensions: [".ts", ".tsx", ".js", ".jsx", ".json", ".png"]
    },
    "target": "node",
    devtool: 'source-map',
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        index: 'index.html'
    }
};
