import * as React from "react";
import { Pokemon } from './Definitions';

interface FeatureProps { pokemon: Pokemon; };

export default class Feature extends React.Component<FeatureProps, {}> {
    render() {
        return (
            <div className="feature">
                Featuring {this.props.pokemon.name}
            </div>
        );
    }
}