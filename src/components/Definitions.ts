export interface PokemonListItem {
    id: number,
    name: string,
    url: string
}

export interface Pokemon {
    name: string,
    habitat: string,
    color: string,
    picture: string,

}
