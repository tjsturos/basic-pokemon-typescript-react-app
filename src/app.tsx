import React = require("react");
import ReactDOM = require("react-dom");

import 'normalize.css/normalize.css';
import './styles/styles.scss';
import Layout from './components/Layout';

ReactDOM.render(
    <Layout />, 
    document.getElementById('app')
);