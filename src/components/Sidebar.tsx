import * as React from "react";
import { PokemonListItem } from './Definitions';

export interface SidebarProps { pokemonList: PokemonListItem[]; changePokemon(id: number) : number; count: number; }


export default class Sidebar extends React.Component<SidebarProps, {}> {

    buttonClick(id: number) {
        console.log("Lookup pokemon number ", id);
    }
    displayList(list: PokemonListItem[]) {
        return list.map((pokemon, index) => {
            return (
                <button key={index} className="sidebar__list-item" onClick={() => this.buttonClick(pokemon.id)}>{pokemon.name}</button>
            )
        })
    }
    render() {
        return (
            <div className="sidebar">
                <h1>Sidebar</h1>
                <div className="list">
                    {this.displayList(this.props.pokemonList)}
                </div>
            </div>
            );
    }
}